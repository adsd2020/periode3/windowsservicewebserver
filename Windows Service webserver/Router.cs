﻿namespace Windows_Service_webserver
{
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Sockets;
    using System.Reflection;

    /// <summary>
    ///     Static class router to handel request and make response for it
    /// </summary>
    internal static class Router
    {
        /// <summary>
        ///     A static property for all routes.
        /// </summary>
        private static readonly Dictionary<string, string> Routes;

        /// <summary>
        ///     Constructor of the class which define and fill Routes property
        /// </summary>
        static Router()
        {
            Routes = new Dictionary<string, string>
            {
                {"/", "Home"},
                {"/form", "Form"},
                {"/result", "Result"},
                {"/mineSweeper", "MineSweeper"}
            };
        }

        /// <summary>
        ///     Handel request and made and return a response for the client
        /// </summary>
        /// <param name="client"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public static Response HandelRequest(Socket client, Request request)
        {
            // control if the header of request not contains "Path" and destination return a response suitable for bad request.
            if (!request.Header.ContainsKey("Path") || !request.Header.ContainsKey("sec-fetch-dest"))
                return new Response(client);

            // Take the destination from header of request
            var acceptType = request.Header["sec-fetch-dest"];

            // Take the Path from Header
            var path = request.Header["Path"];

            // define two variable for content and content type which response wil return to client.
            ResponseContentType contentType;
            string content;

            // fill content type on the base of the read destination of request
            switch (acceptType)
            {
                case string type when type.ToLower() == "document":
                    contentType = ResponseContentType.View;
                    break;
                case string type when type.ToLower() == "image":
                    contentType = ResponseContentType.Image;
                    break;
                case string type when type.ToLower() == "audio":
                    contentType = ResponseContentType.Audio;
                    break;
                case string type when type.ToLower() == "style":
                    contentType = ResponseContentType.StyleSheet;
                    break;
                case string type when type.ToLower() == "script":
                    contentType = ResponseContentType.Script;
                    break;
                default:
                    contentType = ResponseContentType.Unknown;
                    break;
            }

            // Take the content of response on the base of content type
            switch (contentType)
            {
                case ResponseContentType.View:
                    // Look in the Routes if find path give the view name for the path otherwise let it empty
                    var routeView = Routes.ContainsKey(path) ? Routes[path] : "";

                    // if the view name is read from Routes then look if the file exists put the name of view in content otherwise let it empty.
                    content = !string.IsNullOrEmpty(routeView) && View.Exists(routeView) ? routeView : "";
                    break;
                case ResponseContentType.Image:
                case ResponseContentType.Audio:
                case ResponseContentType.StyleSheet:
                case ResponseContentType.Script:
                    // if content type if other than view check if the asked file exists than put the name of file in de content otherwise let it empty
                    content = File.Exists(
                        Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/WebPages" + path) ? path : "";
                    break;
                case ResponseContentType.Unknown:
                default:
                    content = null;
                    break;
            }

            // if content is null or empty or content type is unknown return a response suitable for bad request.
            if (string.IsNullOrEmpty(content) || contentType == ResponseContentType.Unknown)
                return new Response(client);

            // Return a response to can read the content from the file and send it to the client.
            return new Response(content, contentType, client, request);
        }
    }
}
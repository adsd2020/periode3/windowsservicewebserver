﻿namespace Windows_Service_webserver
{
    using System.ServiceProcess;

    /// <summary>
    ///     Claas for start point of program
    /// </summary>
    internal static class Program
    {
        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        private static void Main()
        {
            // Define a object of de type ServiceBase
            ServiceBase[] ServicesToRun;

            // Make a instance op the ServiceBase object and pass my WebServer class to run it as a service
            ServicesToRun = new ServiceBase[]
            {
                new WebServerTeamA1()
            };


#if DEBUG
            // If the project is build in debug mode then run method OnDebug
            ((WebServerTeamA1) ServicesToRun[0]).OnDebug();
#else
            // If the project is build in release mode then let the class ServiceBase run our made object here above as service
            ServiceBase.Run(ServicesToRun);
#endif
        }
    }
}
﻿namespace Windows_Service_webserver
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    ///     Server socket to receive and handle requests of the client sockets.
    ///     <para>is a static class, can not make a instance object of</para>
    /// </summary>
    internal static class SocketServer
    {
        /// <summary>
        ///     the socket attribute to make server socket.
        /// </summary>
        private static Socket _serverSocket;

        /// <summary>
        ///     A list of the asynchronous tasks which received client sockets.
        /// </summary>
        private static List<Task<Socket>> _acceptSockets;

        /// <summary>
        ///     Run the server socket.
        /// </summary>
        public static void Run()
        {
            // make the server socket an instance of class Socket of type Stream and under the Tcp protocol.
            _serverSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);

            // Bind an IpEndPoint to our server socket. (an or a range of id 's of localhost and a port)
            _serverSocket.Bind(new IPEndPoint(IPAddress.Any, 9999));

            // When our server socket is completely busy, then let 100 client socket still wait in a waiting row.  
            _serverSocket.Listen(100);

            // full acceptSockets attribute acceptAsync of server socket
            // which  run a task that wait to receive client socket
            // and when received one return it back as result of that task
            _acceptSockets = new List<Task<Socket>>(new[]
            {
                _serverSocket.AcceptAsync(),
                _serverSocket.AcceptAsync(),
                _serverSocket.AcceptAsync(),
                _serverSocket.AcceptAsync()
            });

            // an infinite loop to receive handel client socket and wait for the next client
            // until the windows service stops.
            while (true)
            {
                // Look at in the acceptSockets List if any task is completed his execution,
                // so af Ant of that tasks in de list is a client received.
                var acceptedClientTask = Task.WaitAny(_acceptSockets.ToArray());

                // give the result of the acceptedClientTask which is a client socket.
                var clientSocket = _acceptSockets[acceptedClientTask].Result;

                // Remove the finished task from acceptSockets list.
                _acceptSockets.RemoveAt(acceptedClientTask);

                // make a buffer of type list of array segment of bytes, to put the received data from client in.
                var buffers = new List<ArraySegment<byte>>
                {
                    new ArraySegment<byte>(new byte[4096]),
                    new ArraySegment<byte>(new byte[4096 * 100]),
                    new ArraySegment<byte>(new byte[4096 * 100]),
                    new ArraySegment<byte>(new byte[4096 * 100]),
                    new ArraySegment<byte>(new byte[4096 * 100])
                };

                // command to network card to put de received data in our buffer and then run a async callback
                // and pass the client socket, the received buffer from client and also the earlier received data for
                // when not received the request in one try and need to take the rest of data.
                clientSocket.BeginReceive(buffers, SocketFlags.None, ReceiveRequest,
                    new Tuple<Socket, List<ArraySegment<byte>>, List<byte>>
                        (clientSocket, buffers, new List<byte>()));

                // add a new Task to acceptSockets list to accept asynchronously a client
                _acceptSockets.Add(_serverSocket.AcceptAsync());
            }
        }

        /// <summary>
        ///     the Async callback which wil run when data from client is received
        /// </summary>
        /// <param name="ar"></param>
        private static void ReceiveRequest(IAsyncResult ar)
        {
            try
            {
                // Take the client socket from state object
                var clientSocket = ((Tuple<Socket, List<ArraySegment<byte>>, List<byte>>) ar.AsyncState).Item1;

                // Take the read buffer from object state
                var receivedBuffer = ((Tuple<Socket, List<ArraySegment<byte>>, List<byte>>) ar.AsyncState).Item2;

                // End the asynchronously receiving data en return the number of received bytes which is put in the buffer.
                var received = clientSocket.EndReceive(ar);

                // Take the earlier read data from object state
                var byteList = ((Tuple<Socket, List<ArraySegment<byte>>, List<byte>>) ar.AsyncState).Item3;

                // Define e local variable to sum number of read bytes from each ArraySegment
                var readBuffers = 0;

                // Get the string of earlier read buffer which is passed in object state
                var message = Encoding.UTF8.GetString(byteList.ToArray());

                // a loop to read each ArraySegment in the buffer
                foreach (var arraySegment in receivedBuffer)
                {
                    // calculate the number of filled bytes from the current ArraySegment.
                    var readThisBuffer = received - readBuffers > arraySegment.Count
                        ? arraySegment.Count
                        : received - readBuffers;

                    // Get string of the bytes in de current ArraySegment
                    var readBuffer = Encoding.UTF8.GetString(arraySegment.Array, 0, readThisBuffer);

                    // Add the bytes in the List of byte of earlier read bytes. we need this to read images from post request.
                    byteList.AddRange(arraySegment.Array.ToList().GetRange(0, readThisBuffer));

                    // Add the read string of bytes from current ArraySegment to the message
                    message += readBuffer;

                    // Add the number of read bytes in the current ArraySegment to readBuffer variable
                    readBuffers += readThisBuffer;

                    // control if sum of read bytes if equal of greater than total received byte in the buffer break the loop
                    // the rest of ArraySegment are ofcourse empty
                    if (received <= readBuffers) break;
                }


                // Control if the message not contains two times newLine then try to received the rest of request from client
                //because the header of each request ended always with two times newLine
                if (!(message.Contains("\r\n\r\n") || message.Contains("\n\n")))
                {
                    clientSocket.BeginReceive(receivedBuffer, SocketFlags.None, ReceiveRequest,
                        new Tuple<Socket, List<ArraySegment<byte>>, List<byte>>(clientSocket, receivedBuffer,
                            byteList));
                }
                else
                {
                    // If is a POST request we need to control if body is also received.
                    if (message.Substring(0, 4).Trim().ToUpper() == "POST" && message.Contains("Content-Length"))
                    {
                        // The header of post request contains content length, find the location of content length
                        var indexLength = message.IndexOf("Content-Length:", StringComparison.Ordinal) + 15;

                        // read value of content length and try to parse it to integer and put it into contentLength variable
                        int.TryParse(message.Substring(indexLength,
                            message.IndexOf("\r\n", indexLength, StringComparison.Ordinal)
                            - indexLength).Trim(), out var contentLength);

                        // Splits message from the first two times new Line into a array of two member and the first member wil be header plus at leased 2 removed new line
                        var bufferHeadLength = message.Split(new[] {"\n\n", "\r\n\r\n"}, 2, StringSplitOptions.None)[0].Length + 2;

                        // if total number of bytes in byteList is fewer than som of calculated header length and read content length, try to read the rest of request.
                        if (byteList.Count < contentLength + bufferHeadLength)
                        {
                            clientSocket.BeginReceive(receivedBuffer, SocketFlags.None, ReceiveRequest,
                                new Tuple<Socket, List<ArraySegment<byte>>, List<byte>>(clientSocket, receivedBuffer,
                                    byteList));
                        }
                        // otherwise is the whole request is received and go further.
                        else
                        {
                            // Send the message and List of bytes to constructor of class request and made a new instance of request.
                            var request = new Request(message, byteList);

                            // Send the client socket and the request to method handelRequest of static class Router which return a instance of class Response.
                            var response = Router.HandelRequest(clientSocket, request);

                            // Run method Send of returned response.
                            response.Send();
                        }
                    }
                    // otherwise it must be a GET request en we handle it as a GET
                    else
                    {
                        // Send the message and List of bytes to constructor of class request and made a new instance of request.
                        var request = new Request(message, byteList);

                        // Send the client socket and the request to method handelRequest of static class Router which return a instance of class Response.
                        var response = Router.HandelRequest(clientSocket, request);

                        // Run method Send of returned response.
                        response.Send();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR " + e.Message);
            }
        }
    }
}
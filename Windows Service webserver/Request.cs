﻿namespace Windows_Service_webserver
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    ///     Request object of received bytes and message from client socket
    /// </summary>
    public class Request
    {
        /// <summary>
        ///     Constructor of the class received an analyze the request bytes and full the request object
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="bufferBytes"></param>
        public Request(string buffer, List<byte> bufferBytes)
        {
            // Control if buffer not contains two times newLine in a row then go back because it's not valid request
            if (!((buffer.Contains("\n\n") || buffer.Contains("\r\n\r\n")) && buffer.Contains("Host:") &&
                  buffer.Contains("HTTP") && buffer.Contains("Connection:")))
                return;

            try
            {
                // splits the buffer from the two new line in a row and every thing befor that wil be the header
                var httpHeaderString = buffer.Split(new[] {"\n\n", "\r\n\r\n"}, 2, StringSplitOptions.None)[0];

                // Splits the header and put each row into an array
                var httpHeader = httpHeaderString.Split(new[] {"\n", "\r\n"}, StringSplitOptions.RemoveEmptyEntries);

                // Take the first element in header array and splits it by space,
                // so we can figure out the method of request, asked path, protocol type and it's version.
                var httpQuery = httpHeader[0].Split(' ');
                Header.Add("Commands", httpQuery[0].ToLower().Trim());
                Header.Add("Path", httpQuery[1]);
                Header.Add("ProtocolName", httpQuery[2].Split(new[] {'/'}, 2)[0]);
                Header.Add("ProtocolVersion", httpQuery[2].Split(new[] {'/'}, 2)[1]);

                // Loop through the rest of elements in the header array and splits then to find out the rest of element in header
                foreach (var line in httpHeader.Skip(1))
                    Header.Add(line.Split(new[] {':'}, 2)[0].ToLower().Trim(),
                        line.Split(new[] {':'}, 2)[1].Trim());

                // If the method of request is not POST or the header not contains a content-type
                // then it should be Get and therefor is header al enough, so go back.
                if (Header["Commands"].ToUpper() != "POST" || !Header.ContainsKey("content-type")) return;

                // On the base of content type read the parameters and/or also files
                if (Header["content-type"] == "application/x-www-form-urlencoded")
                {
                    // Take every thing after the two new line in a row and they wil be the body of post request
                    // by splitting of them find out the parameters
                    // body wil be just in one line and every parameter is separated by &
                    // the name and value also separated with =
                    var paramLine = buffer.Split(new[] {"\n\n", "\r\n\r\n"}, 2, StringSplitOptions.None)[1]
                        .Split(new[] {"&"}, StringSplitOptions.None);

                    foreach (var line in paramLine)
                    {
                        var param = line.Split(new[] {"="}, 2, StringSplitOptions.RemoveEmptyEntries);
                        var key = param.Length > 0 ? param[0] : "";
                        var value = param.Length > 1 ? param[1] : "";

                        Parameters.Add(key, value);
                    }
                }
                else if (Header["content-type"].Contains("multipart/form-data; boundary="))
                {
                    // If post request is sent multipart then every parameter of the body is separated bij a WebkitBoundary line which can be find in de header
                    // Remove the first lines if they are new line or recursive new line
                    bufferBytes.RemoveRange(0, httpHeaderString.Length + 2);
                    while (bufferBytes[0] == 13 || bufferBytes[0] == 10)
                        bufferBytes.RemoveAt(0);

                    // find out what will be the separation line of each element in body
                    var boundary = "--" + Header["content-type"].Split(';')[1].Split('=')[1];

                    // find the first boundary line, the line after this wil be the start of a parameter.
                    var startIndex =
                        Encoding.UTF8.GetString(bufferBytes.ToArray()).IndexOf(boundary, StringComparison.Ordinal) +
                        boundary.Length;

                    // Infinite loop to find all parameters of the body, it wil stop inside the loop self, if all parameters are found.
                    while (true)
                    {
                        // remove the new line or recursive new line
                        while (bufferBytes[startIndex] == 13 || bufferBytes[startIndex] == 10)
                            startIndex++;

                        // Take the first line in current section
                        var firstLine = Encoding.UTF8
                            .GetString(bufferBytes
                                .GetRange(startIndex, bufferBytes.IndexOf(10, startIndex) - startIndex).ToArray()).Trim('\r');

                        // If the first line is not equal to "Content-Disposition:" , then is the end of parameters achieved and can out of the loop
                        if (firstLine.Substring(0, 20) != "Content-Disposition:") break;

                        // Find the name of the parameter out from first line
                        var paramName = firstLine.Split(';')[1].Split('=')[1].Trim('\r').Trim('"');

                        // If the fiest line not contains "; filename=" then the parameter is just a variable in body
                        if (!firstLine.Contains("; filename="))
                        {
                            // Find the end of first line where begin a newline
                            var startValue = bufferBytes.IndexOf(10, startIndex + firstLine.Length);

                            // If ther is still newline or recursive new line remove them until start of the value
                            while (bufferBytes[startValue] == 13 || bufferBytes[startValue] == 10)
                                startValue++;

                            // Read the value of the parameter
                            var value = Encoding.UTF8
                                .GetString(bufferBytes
                                    .GetRange(startValue, bufferBytes.IndexOf(10, startValue) - startValue).ToArray()).Trim('\r');

                            // Set start index to find the next section, if value of current section was empty take the startValue otherwise search for first new line.
                            startIndex = (value.Contains(boundary) ? startValue : bufferBytes.IndexOf(10, startValue)) +
                                         boundary.Length + 1;

                            // Also if the read value contains boundary text it means that value was empty and herr we correct this.
                            if (value.Contains(boundary))
                                value = "";

                            // Add name and value of the parameter to parameter property
                            Parameters.Add(paramName, value.Length > 0 ? value : "");
                        }
                        // otherwise the parameter of this section wil be a sent file
                        else
                        {
                            // If parameter is a file will the second line contains information about the type/mime of the file
                            // same as first line find the start position of the second line and read it to first newLine character
                            // and then via splitting find the type of the file
                            var startType = bufferBytes.IndexOf(10, startIndex + firstLine.Length);
                            while (bufferBytes[startType] == 13 || bufferBytes[startType] == 10)
                                startType++;
                            var typeLine = Encoding.UTF8.GetString(bufferBytes
                                .GetRange(startType, bufferBytes.IndexOf(10, startType) - startType).ToArray());
                            var mime = typeLine.Split(':')[1].Trim('\r').Trim();

                            // Find the start index of the file and remove extra newLine or recursive newLine character until beginning of the file content
                            var startFile = bufferBytes.IndexOf(10, startType + typeLine.Length);
                            while (bufferBytes[startFile] == 13 || bufferBytes[startFile] == 10)
                                startFile++;

                            // make a loop through the content of the file and define two variable to stop the loop when end of the section achieved
                            // !! DE CONTENT OF THE FILE WILL READ OUT THE BUFFER BYTE, BECAUSE THE FILE CONTENT WILL DAMAGE IF IT ENCODED TO UTF8 STRING !!
                            var endFile = -1;
                            var startBoundary = startFile;
                            while (endFile < 0 && startBoundary >= startFile)
                            {
                                // Because boundary start with byte 45 ("-") search for the first byte which is 45
                                // and control if that is the beginning of the boundary, in this case is founded byte 45 
                                // is the end of file content and beginning of the boundary text separator
                                // otherwise search for the next byte 45
                                startBoundary = bufferBytes.IndexOf(45, startBoundary);
                                var boundaryCheck =
                                    Encoding.UTF8.GetString(bufferBytes.GetRange(startBoundary, boundary.Length)
                                        .ToArray());

                                if (boundaryCheck == boundary)
                                    endFile = startBoundary;
                                else
                                    startBoundary++;
                            }

                            // Define a empty variable and check if the end file is greater than start of the file,
                            // then put the file content therein. otherwise the file content wil stay empty.
                            var content = "";
                            if (endFile > startFile)
                            {
                                // Take the content of the file and remove the extra newLine or recursive newLine character from the end of content
                                var contentBytes = bufferBytes.GetRange(startFile, endFile - startFile);
                                while (contentBytes[contentBytes.Count - 1] == 10 ||
                                       contentBytes[contentBytes.Count - 1] == 13)
                                    contentBytes.RemoveAt(contentBytes.Count - 1);

                                // Convert the bytes to a Base 64 string and put it into content.
                                content = Convert.ToBase64String(contentBytes.ToArray());
                            }

                            // Make a dictionary and put the type and content of the file therein and add it into the file property.
                            var uploadedFile = new Dictionary<string, string>
                            {
                                {"content", content},
                                {"mime", mime}
                            };

                            Files.Add(paramName, uploadedFile);

                            // Set the startIndex and search the next parameter section
                            startIndex = endFile + boundary.Length;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        ///     Property header van request
        /// </summary>
        public Dictionary<string, string> Header { get; } = new Dictionary<string, string>();

        /// <summary>
        ///     Property parameters of post request
        /// </summary>
        public Dictionary<string, string> Parameters { get; } = new Dictionary<string, string>();

        /// <summary>
        ///     Property files send with request
        /// </summary>
        public Dictionary<string, Dictionary<string, string>> Files { get; } =
            new Dictionary<string, Dictionary<string, string>>();
    }
}
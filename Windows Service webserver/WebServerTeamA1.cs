﻿namespace Windows_Service_webserver
{
    using System.ServiceProcess;
    using System.Threading;

    /// <summary>
    ///     Webserver as a windows service
    ///     <inheritdoc cref="ServiceBase" />
    /// </summary>
    public partial class WebServerTeamA1 : ServiceBase
    {
        /// <summary>
        ///     Define the main thread of windows service
        /// </summary>
        private Thread _thread;

        /// <summary>
        ///     Constructor of the class
        /// </summary>
        public WebServerTeamA1()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Self added method to can run it as console app to debug it.
        /// </summary>
        public void OnDebug()
        {
            OnStart(null);
        }

        /// <summary>
        ///     OnStart method which will run when our windows service is started.
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            // Make a instance of thread for the main thread attribute of the windows service and run the method 'Run' of static class SocketServer
            _thread = new Thread(SocketServer.Run);

            // Start the main thread attribute.
            _thread.Start();
        }

        /// <summary>
        ///     OnStop method which will run when our windows service will stop.
        /// </summary>
        protected override void OnStop()
        {
            // Abort the main thread attribute.
            _thread.Abort();
        }
    }
}
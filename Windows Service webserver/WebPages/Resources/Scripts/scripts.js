﻿document.addEventListener("DOMContentLoaded", function (event) {
    let draggableWindow = document.querySelector('.window__container');
    let draggableHeader = document.querySelector('.window__header');
    let isDragging = false;
    let isScaling = false;
    let offset = {
        x: 0,
        y: 0
    }
    let clickEvent = null
    let x, y, b
    let minWidth = 450;
    let minHeight = 200;
    let MARGINS = 10
    let onRightEdge, onBottomEdge, onLeftEdge, onTopEdge;

    draggableWindow.addEventListener('mousedown', (e) => {
        isScaling = (onRightEdge || onBottomEdge || onTopEdge || onLeftEdge);

        if(!isScaling && e.target === draggableHeader) {
            isDragging = true;
            offset.x = e.offsetX;
            offset.y = e.offsetY;
        } else {
            clickEvent = {
                cx: e.clientX,
                cy: e.clientY,
                w: b.width,
                h: b.height,
                onTopEdge: onTopEdge,
                onLeftEdge: onLeftEdge,
                onRightEdge: onRightEdge,
                onBottomEdge: onBottomEdge
            }
        }
    })

    window.addEventListener('mouseup', (e) => {
        isDragging = false;
        isScaling = false;
        clickEvent = null
    })

    const positionHandler = (e) => {
        if (isDragging) {
            draggableWindow.style.top = (e.pageY - offset.y) + 'px'
            draggableWindow.style.left = (e.pageX - offset.x) + 'px'
        }

        getWindowMousePosition(e)
        
        if(isScaling) {
            resize(e)
        } else {
            if (onRightEdge && onBottomEdge || onLeftEdge && onTopEdge) {
                draggableWindow.style.cursor = 'nwse-resize';
            } else if (onRightEdge && onTopEdge || onBottomEdge && onLeftEdge) {
                draggableWindow.style.cursor = 'nesw-resize';
            } else if (onRightEdge || onLeftEdge) {
                draggableWindow.style.cursor = 'ew-resize';
            } else if (onBottomEdge || onTopEdge) {
                draggableWindow.style.cursor = 'ns-resize';
            } else {
                draggableWindow.style.cursor = 'initial';
            }
        }
    }

    const getWindowMousePosition = (e) => {
        b = draggableWindow.getBoundingClientRect();
        x = e.clientX - b.left;
        y = e.clientY - b.top;

        onTopEdge = y < MARGINS;
        onLeftEdge = x < MARGINS;
        onRightEdge = x >= b.width - MARGINS;
        onBottomEdge = y >= b.height - MARGINS;
    }

    const resize = (e) => {
        if (clickEvent.onRightEdge) draggableWindow.style.width = Math.max(x, minWidth) + 'px';
        if (clickEvent.onBottomEdge) draggableWindow.style.height = Math.max(y, minHeight) + 'px';
    
        if (clickEvent.onLeftEdge) {
          var currentWidth = Math.max(clickEvent.cx - e.clientX  + clickEvent.w, minWidth);
          if (currentWidth > minWidth) {
            draggableWindow.style.width = currentWidth + 'px';
            draggableWindow.style.left = e.clientX + 'px';	
          }
        }
    
        if (clickEvent.onTopEdge) {
          var currentHeight = Math.max(clickEvent.cy - e.clientY  + clickEvent.h, minHeight);
          if (currentHeight > minHeight) {
            draggableWindow.style.height = currentHeight + 'px';
            draggableWindow.style.top = e.clientY + 'px';	
          }
        }    
    }

    window.top.addEventListener('mousemove', positionHandler)
})

const toggleWindow = () => {
    let window = document.querySelector('.window__container');
    let nav = document.getElementById('current-tab')
    
    if(window.classList.contains('hidden')) {
        window.classList.remove('hidden')
        nav.classList.add('nav__list-item--active') 
    } else {
        window.classList.add('hidden')
        nav.classList.remove('nav__list-item--active') 
    }
}

const error = () => {
    let error = document.getElementById('error');
    error.currentTime = 0
    error.play();
}

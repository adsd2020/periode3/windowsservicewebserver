﻿namespace Windows_Service_webserver
{
    using System;
    using System.IO;
    using System.Reflection;

    /// <summary>
    ///     A static class to do actions with view files
    /// </summary>
    internal static class View
    {
        /// <summary>
        ///     Get the content of the view
        /// </summary>
        /// <param name="pageName"></param>
        /// <returns></returns>
        public static string Content(string pageName)
        {
            // Find the path where this windows service is installed and is run
            var currentLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            // find the path of the view.
            var filePath = currentLocation + $"/WebPages/{pageName}.html";

            try
            {
                // If the view is not exist return the content of "ViewNotFound"
                if (!File.Exists(filePath))
                    return File.ReadAllText(currentLocation + "/WebPages/ViewNotFound.html");

                // return the content of the view
                return File.ReadAllText(filePath);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        /// <summary>
        ///     Check if the view exist
        /// </summary>
        /// <param name="pageName"></param>
        /// <returns></returns>
        public static bool Exists(string pageName)
        {
            // Find the path where this windows service is installed and is run
            var currentLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            // Figure out what the path of the view is.
            var filePath = currentLocation + $"/WebPages/{pageName}.html";
            try
            {
                // return if the file exist
                return File.Exists(filePath);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
    }
}
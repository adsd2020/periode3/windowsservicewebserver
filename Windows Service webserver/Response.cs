﻿namespace Windows_Service_webserver
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Net.Sockets;
    using System.Reflection;
    using System.Text;

    /// <summary>
    ///     Class response to send it to client
    /// </summary>
    public class Response
    {
        /// <summary>
        ///     Constructor to answer a request which is perhaps a good request
        /// </summary>
        /// <param name="contentPath"></param>
        /// <param name="contentType"></param>
        /// <param name="clientSocket"></param>
        /// <param name="request"></param>
        public Response(string contentPath, ResponseContentType contentType, Socket clientSocket, Request request)
        {
            // Put the client socket into the Client property
            Client = clientSocket;

            // Put the content type into the contentType property
            ContentType = contentType;

            // Set property BadRequest to False
            BadRequest = false;


            // If the content type ie View, try to pick the view up, otherwise it must send a file,
            // try to pick the file up.
            switch (contentType)
            {
                case ResponseContentType.View:
                    // Pick the view up and if the result is null pick the content of the viewNotFound.
                    var view = View.Content(contentPath) ?? View.Content("ViewNotFound");

                    // Loop through the parameters of request and for each parameter try to replace [ "{" + Key of the parameter + "}" ] with it's value
                    // and repeat this again for each file and put the content of the file into it's place.
                    // for this we use Aggregate query from Linq library
                    view = request.Parameters.Aggregate(view,
                        (current, requestParameter) =>
                            current.Replace("{" + requestParameter.Key + "}", requestParameter.Value));

                    view = request.Files.Aggregate(view,
                        (current, requestFile) => 
                            current.Replace("{" + requestFile.Key + "}", "data:image/png;base64," + requestFile.Value["content"]));

                    // Encode the view content to an array of byte and put it to Content property
                    Content = Encoding.UTF8.GetBytes(view);
                    break;
                case ResponseContentType.Image:
                case ResponseContentType.Audio:
                case ResponseContentType.StyleSheet:
                case ResponseContentType.Script:
                    // find the path of the file which should be sent
                    // on the base of the location where this windows service is installed and wil run.
                    var relPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/WebPages" + contentPath;

                    // Controle if the file is not exist set the Content property to an empty array of bytes and break the switch out
                    if (!File.Exists(relPath))
                    {
                        Content = new byte[] { };
                        break;
                    }

                    // Pick up the info of the file and make an array of byte
                    // with a length of the file to put the content of the file into it.
                    var fileInfo = new FileInfo(relPath);
                    var fileContent = new byte[fileInfo.Length];

                    // Make a fileStream to the file and open the stream to read his content.
                    var fileReader = new FileStream(relPath, FileMode.Open, FileAccess.Read);

                    // Read the content of the file and put it into the fileContent array of bytes
                    fileReader.Read(fileContent, 0, fileContent.Length);

                    // Close the file stream
                    fileReader.Close();

                    // Put the read content into Content property
                    Content = fileContent;
                    break;
                case ResponseContentType.Unknown:
                default:
                    // If the content type in others then pick the content of "ViewNotFount" and put it into the property Content
                    Content = Encoding.UTF8.GetBytes(View.Content("ViewNotFound"));
                    break;
            }
        }

        /// <summary>
        ///     Constructor to answer a bad request
        /// </summary>
        /// <param name="clientSocket"></param>
        public Response(Socket clientSocket)
        {
            // Put the client socket to the Client property
            Client = clientSocket;

            // Pick up the "BadRequest" view and put it into Content property
            Content = Encoding.UTF8.GetBytes(View.Content("BadRequest"));

            // Set property BadRequest to True.
            BadRequest = true;
        }

        /// <summary>
        ///     Private property for the header of the response
        /// </summary>
        private string Header { get; set; }

        /// <summary>
        ///     Private property for the content of the response as a byte array
        /// </summary>
        private byte[] Content { get; }

        /// <summary>
        ///     Private property for the type of the content which wil be send to client
        /// </summary>
        private ResponseContentType ContentType { get; }

        /// <summary>
        ///     The client who wil receive the response
        /// </summary>
        private Socket Client { get; }

        /// <summary>
        ///     An extra property to determine if the request was a bad request.
        ///     specially for the status code of response.
        /// </summary>
        private bool BadRequest { get; }

        /// <summary>
        ///     Send the response to client
        /// </summary>
        public void Send()
        {
            // Here using client because at the end of this method we are done with this client socket
            // and it can be removed from the memory of our server
            using (Client)
            {
                // make the header of the response
                var date = DateTime.UtcNow.ToString("R");
                Header = BadRequest
                    ? "HTTP/1.1 400 Bad Request\r\n"
                    : Content.Length > 0 && ContentType != ResponseContentType.Unknown
                        ? "HTTP/1.1 200 OK\r\n"
                        : "HTTP/1.1 204 No Content\r\n";
                Header += $"Date:{date}\r\n";
                Header += "Server: Opdracht3 Team A1";
                Header += $"Content-Length:{Content.Length}\r\n";
                Header += "Connection: Closed\r\n\r\n";

                // Encode the header and send it to the client.
                Client.Send(Encoding.UTF8.GetBytes(Header));

                // Send the content of the response to the client.
                Client.Send(Content);
            }
        }
    }

    /// <summary>
    ///     The enum for the content type of response
    /// </summary>
    public enum ResponseContentType
    {
        View,
        Image,
        Audio,
        StyleSheet,
        Script,
        Unknown
    }
}
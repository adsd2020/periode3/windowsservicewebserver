# TeamA1 Webserver

## Requirements
To run and setup this application the following items are required and need to be installed:

.NET Framework : https://dotnet.microsoft.com/download

## Installation

* Open the projectfile and built the projct once 
* Open your command line in administrator mode
* Locate the project file directory: <br />
  `C:\Users\Joeghanoe\source\repos\{projectname}\{projectname}\bin\Debug`
* Run the following command in on this location:<br />
  `c:\Windows\Microsoft.NET\Framework64\v4.0.30319\installUtil.exe {projectname}.exe`
* Run the project in Visual Studio


## Running
Simply press the run button in Visual Studio basic